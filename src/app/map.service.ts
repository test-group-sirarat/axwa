import { ElementRef, Injectable } from '@angular/core';
import Map from "@arcgis/core/Map";
import MapView from "@arcgis/core/views/MapView";
import Graphic from "@arcgis/core/Graphic";
import SimpleMarkerSymbol from "@arcgis/core/symbols/SimpleMarkerSymbol";
import FeatureLayer from "@arcgis/core/layers/FeatureLayer";
import Point from "@arcgis/core/geometry/Point";



@Injectable({
  providedIn: 'root'
})
export class MapService {

  map!: Map
  mapView!: MapView
  featureLayer!: FeatureLayer

  lat:number
  long: number




  constructor() { }

  initialMap(dom: ElementRef) {
    this.map = new Map({
      basemap: "topo-vector"
    })

    this.mapView = new MapView({
      map: this.map,
      container: dom.nativeElement,
      center: [	100.5, 13.7],
      zoom: 6
    })

    this.mapView.when(()=>{
      this.mapView.on("click",(res)=>{
        console.log('res : ',res)
        this.mapView.graphics.removeAll()
        this.goTo(Number(res.mapPoint.latitude),Number(res.mapPoint.longitude))
        console.log("lat",res.mapPoint.latitude)
        console.log("long", res.mapPoint.longitude)
      })


    })
   

  
  }

  goTo(lat: number, long: number) {
    this.mapView.goTo({
      center: [long, lat],
      zoom: 8
    }, {
      duration: 3000
    })

    this.mapView.graphics.removeAll()
    //Add graphic
    const point = new Point({
      latitude: lat,
      longitude: long
    })

    const symbol = new SimpleMarkerSymbol({
      style: "circle",
      color: [0, 255, 0, 0, 0.5],
      size: "15px", //pixels
      outline: {//autocasts as new SimpleLineSymbol()
        color: [255, 0, 0],
        width: 3 //points
      }
    })

    const graphic = new Graphic({
      geometry: point,
      symbol: symbol
    })
    this.mapView.graphics.add(graphic)
  }

  reMap() {
    this.mapView.goTo({
      center: [107,13.5],
      zoom: 4
    }, {
      duration: 200
     
    })
    this.mapView.graphics.removeAll()

 
    
  }

  addMap(){
    
   

  }



  
}