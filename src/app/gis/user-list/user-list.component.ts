import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AxRequestService } from '@atlasx/core/http-service'
import { MapService } from 'src/app/map.service';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  @ViewChild('divMap') divMap!: ElementRef
 

  

  data: { NAME: string, SURNAME: string, MOBILE: string, GENDER: string, USER_ID: number, LATITUDE: number, LONGITUDE: number }[] = []
  @Input() user_ID: number
  @Input() name!: string
  @Input() surname!: string
  @Input() mobile!: string
  gender!: string
  @Input() lat!: number
  @Input() long!: number

  
  searchText!: ''

  

  check: number = 0
  onClick: boolean = false
  count: number = 0

  filteredCount = { count: 0 };
  @Input() countFilter!:number

  
  TextEditAndAdd: string = ''

  ////
  filterMetadata = { count: 0 };
  filtre: string;

  


  genders: Gender[];

  selectedGender: string;

  checkClear: number

  constructor(private requesService: AxRequestService, private mapService: MapService) {
    this.genders = [
      {gender : 'MEN' , code:'M'},
      {gender : 'WOMEN' , code:'W'},

    ]
   }

  ngOnInit(): void {
    this.checkClear = 0
    this.testSP()
    this.TextEditAndAdd = "เพิ่มข้อมูลผู้ใช้งาน"
    
  }

  ngAfterViewInit() {
    this.mapService.initialMap(this.divMap)
    this.mapService.mapView.on("click", (res) => {
      this.lat = Number(res.mapPoint.latitude)
      this.long = Number(res.mapPoint.longitude)
      console.log('lat-long form user.ts :', this.lat, this.long)
    })
  }
  testSP() {
    console.log("TestSP")
    this.count = 0
    this.data.length = 0
    let params = {

    }
    this.requesService.sp("TEMP_USER_Q", "POST", params).toPromise().then((response) => {
      console.log("requestService response = ", response)
      if (response["data"].length > 0) {
        for (let user of response["data"]) {
          this.data.push({ NAME: user.NAME, SURNAME: user.SURNAME, MOBILE: user.MOBILE, GENDER: user.GENDER, USER_ID: user.USER_ID, LATITUDE: user.LATITUDE, LONGITUDE: user.LONGITUDE })
          // this.count++;
          console.log(this.count)
          this.mapService.reMap()
        }
      }
      console.log(this.data)
    })
  }

  clearData() {
    this.name = ''
    this.surname = ''
    this.mobile = ''
    this.gender = ''
    this.mapService.reMap()
  }

  ClickName(USER_ID: number) {
    this.checkClear = 1
    this.onClick = true
    this.TextEditAndAdd = "แก้ไขผู้ใช้งาน"
    for (let user of this.data) {
      if (USER_ID == user.USER_ID) {
        this.user_ID = user.USER_ID
        this.name = user.NAME
        this.surname = user.SURNAME
        this.mobile = user.MOBILE
        this.gender = user.GENDER
        this.lat = user.LATITUDE
        this.long = user.LONGITUDE
        this.mapService.goTo(Number(this.lat), Number(this.long))
        console.log("lat", this.lat)
        console.log("long", this.long)
      }
    }
  }
  delUser(user_ID: number) {
    let params = {
      USER_ID: user_ID,
    }
    console.log("delUser response = ", params)

    this.requesService.sp("TEMP_USER_D", "GET", params).toPromise().then((response) => {
      console.log("delUser response = ", response)
      this.clearData()
      this.onClick = false
      this.testSP()
      this.AddData()
      this.mapService.reMap()
    })
  }




  UpdateAndAddUser(user_id: number, name: string, surname: string, mobile: string, gender: string , lat: number, long:number) {
    for (let user of this.data) {
      if (user_id == user.USER_ID) {
        this.check = user_id
      }
    }
    if (this.check == user_id) {
      let params = {
        USER_ID: user_id,
        NAME: name,
        SURNAME: surname,
        MOBILE: mobile,
        GENDER: this.gender,
        LATITUDE: this.lat,
        LONGITUDE: this.long
      }
      console.log("update response = ", params)
      this.requesService.sp("TEMP_USER_U", "GET", params).toPromise().then((response) => {
        console.log("update response = ", response)
        this.clearData()
        this.testSP()
        // this.returnMap()
        this.AddData()
        this.mapService.reMap()
        

      })
    } else if (this.check == 0) {
      let params = {
        USER_ID: user_id,
        NAME: name,
        SURNAME: surname,
        MOBILE: mobile,
        GENDER: this.gender,
        LATITUDE: this.lat,
        LONGITUDE: this.long
      }
      console.log("addUser response = ", params)

      this.requesService.sp("TEMP_USER_I", "GET", params).toPromise().then((response) => {
        console.log("addUser response = ", response)
        this.clearData()
        this.onClick = false
        this.testSP()
        this.mapService.reMap()
      })
    }

  }
  AddData() {
    this.checkClear = 0
    this.clearData()
    this.onClick = false
    this.TextEditAndAdd = "เพิ่มข้อมูลผู้ใช้งาน"
    this.mapService.reMap()
  }



  alertDelete() {
    alert("ยืนยันการลบ");
  }




 
  countPipe(searchText: string ){
  
  
   
  }



  



  
  trackElement(index: number, element: any) {
    return element ? element.id : null;
  }
  
}
 

interface Gender {
  gender: string,
  code : string
  
}


