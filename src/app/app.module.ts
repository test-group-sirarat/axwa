import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module'
import { AxAuthenticationModule } from '@atlasx/core/authentication'
import { AxConfigurationModule } from '@atlasx/core/configuration'
import { AxWebServiceUrl } from '@atlasx/core/http-service'
import { AppComponent } from './app.component'
import { environment } from '../environments/environment'
import { ArcgisjsapiProvider } from './gis/argisjsapi-provider';
import { UserListComponent } from './gis/user-list/user-list.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {DropdownModule} from 'primeng/dropdown';
import { SearchPipe } from './gis/user-list/search.pipe';






@NgModule({
  declarations: [AppComponent, UserListComponent, SearchPipe],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    ButtonModule,
    InputTextModule,


    // Required register, if application use AtlasX configuration pattern.
    // It will load configuration before application initial startup.
    AxConfigurationModule,
    Ng2SearchPipeModule,

    // Required register, if application use authentication.
    AxAuthenticationModule.forRoot(environment),
    DropdownModule
  ],
  providers: [
    // Required register, if application use AxAuthenticationModule or AxConfigurationModule.
    { provide: AxWebServiceUrl, useValue: environment.webServiceUrl },

    // Requried register, if application use ArcGIS API for JavaScript.
    ArcgisjsapiProvider,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
